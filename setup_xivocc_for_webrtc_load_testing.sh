#!/bin/bash

WORKER_CONNECTIONS=2048
NGINX_CONF="/etc/docker/compose/nginx.conf"
YAML="/etc/docker/compose/docker-xivocc.override.yml"

function override_nginx_conf() {
  docker cp xivocc_nginx_1:/etc/nginx/nginx.conf $NGINX_CONF
  if [ -f "${NGINX_CONF}" ]; then
    echo "Overriding nginx.conf to enable more worker connections"
    sed -i "s/worker_connections  1024;/worker_connections  ${WORKER_CONNECTIONS};/" $NGINX_CONF
  else
    echo "Failed to get nginx.conf from container."
    exit 1
  fi
}

function add_nginx_override_in_yaml() {
  if [ -f "${YAML}" ]; then
    echo "Adding nginx.conf override in yaml file."
    echo "  nginx:" >> $YAML
    echo "    volumes:" >> $YAML
    echo "    - ${NGINX_CONF}:/etc/nginx/nginx.conf" >> $YAML
  else
    echo "Missing ${YAML}"
    exit 1
  fi
}

override_nginx_conf
add_nginx_override_in_yaml # check docker-xivocc.override.yml, it can break this script.
xivocc-dcomp up -d