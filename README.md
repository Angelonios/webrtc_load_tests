# Webrtc load tests
This project is used to run multiple webrtc load test scenarios against XiVO platform.  
It uses these projects:  
`webrtc_load_tests_webpack`  
`webrtc_load_tests_puppeteer`  
`webrtc_load_tests_client_proxy`  
The core of the load tests is the `puppeteer` container, which controls real browser instances. 
The `webpack` service serves simple web pages with minimal javascript that automates calling from the browser.  
The `client_proxy` collects call events from the browsers and synchronizes them.  
Running multiple load test scenarios at once, requires to run multiple `puppeteer` services.  
Other two services `webpack` and `client_proxy` are capable to handle any number of browsers at once and  
are not dependend on the `puppeteer` container anyway.

# Load test preconditions

## Scenarios overview

Currently this framework supports these scenarios (with their template names):  
*Note: Do not change existing templates names, since it can breake the load tests.*  

1. webrtc calls with multiple users (multiple_users) 
1. webrtc simultaneous calls with just two users (multiple_calls) 
1. webrtc register (no template available) 

The first scenario loads multiple users and makes them call each other.  
*Note: this scenario is easy to setup, but it uses a lot of resources,  
since it simulates real end users with browsers.*  

The second scenario loads only two users, and they generate a lot of  
simultaneous calls.  
*Note: this scenario requires extensive configuration of XiVO platform,  
but also it's the less resource intensive as opposed to the first one.*  

Third scenario loads multiple users and only registers them.

## Prepare XiVO platform

In case you want to use the first scenario (multiple_users), or the third (register),  
make sure you have properly configured users on your XiVO.  

In case you want to use the second scenario (multiple_calls), please refer to the  
documentation included [here](https://projects.xivo.solutions/issues/5884).  

**WARNING: please make sure that all your users that have _CTI accounts_ have the  
_same password!_ This framework currently is missing feature to import csv with   
usernames and passwords.**

## Specify scenarios

To specify scenario, first copy the templates from tempaltes folder (eg. `compose_template`,  
`load_test_config`, `puppeteer_service_template`, `multiple_users.scenario` and/or  
`multiple_calls.scenario`) to your config folder.

The templates have predefined values, which should not be changed in normal circumstances.  
Other variables relate to the network configuration of your XiVO platform.

Make sure that TARGET_VERSION is set to the branch name of this project, that contains a  
stable version. (By default it should be `master`)

### Browser modes

Browser modes are just options for how to run the load test. Here is the current list:  

- 0 - One Browser For All (Each user is loaded in a separate context, with a tab in one browser)  
**Note: this mode is suitable for the simultaneous calls scenario.**  

- 1 - Two Browsers One Context Per User (On one browser are the callers, on the other are callees)  
**Note: this mode is suitable for the simultaneous calls scenario.**  

- 2 - Two Browsers One Context Per Caller (Same as 1)  
**Note: this mode is suitable if you have one callee that can do simultaneous calls, and many callers**  

- 3 - Load Only Callers (Same as 1 & 2, except there are no callees)  
**Note: this mode is used for calling own devices with the framework**  

- 4 - Everyone Has Browser (Each user get's own browser process)  
**Note: this mode is resource intensive! Do not use in scenarios!**  

- 5 - One Browser For Four Pairs (For every 8 users spawn one browser)  
**Note: this mode is suitable for the multiple users calls scenario**  

### Debugging scenarios

It's possible to connect to remote chrome instances eg. to see the devtools, but you can debug with  
max 2 browsers. In case you debug a scenario with more than 2 browsers it will crash.  

The reason is that chrome has a debugging port, and it's not possible to debug more than 3  
browsers from one port. We could add different port to each browser, but then we will face complexities  
like exposing all those ports in the puppeteer containers, and others. 

# Usage

## In load testing production
This framework is indented to run in cycles to maintain certain level of load on a XiVO platform.  
The main two launching options are then:

- `./docker_build`  
Creates from provided configuration the .env and docker-compose files. Then it pulls docker  
images and recreates the containers.  
- `./docker_build --restart OR ./docker_build -r`  
Recreates only the puppeteer containers that do the webrtc calls.  
**Note: don't use this command to restart the whole framework, check the development commands.**

## In development
For development and debugging purposes there are these additional launching options:
- `./docker_build`  
In development context it's better to use this command to restart the load tests.  
- `./docker_build --build OR ./docker_build -b`  
Used in case any code in the load test containers was changed.  
- `./docker_build --clean OR ./docker_build -c`  
Used in case of upgrading the images used in the framework.  

# Logging
The stdout of the compose containers is the only log available.  

Call reports created by the phone events from websockets are available from the client proxy  
service at:

`http://LOADTESTER_URL:CLIENT_PROXY_PORT/get`  

This url will return a json file containing: Iterations > calls > call events with timestamps.  
Note that loadtest reports are deleted after you stop the client proxy container.

# Example scenarios

In section there will be example configs for different scenarios.

## Multiple users scenario

File: `load_test_config`

```
SUT_FQDN=myedge.dev.avencall.com
XIVOCC_IP=192.168.32.15
XIVO_IP=192.168.32.10
MDS1_IP=192.168.32.12
MDS2_IP=192.168.32.13
DEBUG=false
DEFAULT_USER_PWD=secret
TARGET_VERSION=master
CLIENT_PROXY_PORT=48576
WEBPACK_PORT=8115
DEBUG_PORT=9222
LOADTEST_URL=localhost
PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
PUPPETEER_EXECUTABLE_PATH=/puppeteer-controller/node_modules/puppeteer/.local-chromium/chrome-linux/chrome
```

File: `multiple_users.scenario`

```
STARTING_PHONE_NUMBER=9000
NUMBER_OF_USERS=96
SCENARIO_CONDITION=caller.EventReleased
BROWSER_MODE=5
WEBRTC_LOADTEST_ITERATIONS=3
AVERAGE_TIME_TO_CALL_MS=120000
DELAY=500
CALL_LENGTH_MS=5000
DELAY_BETWEEN_CALLS=3000
```

## Multiple calls scenario

File: `load_test_config`

```
SUT_FQDN=myedge.dev.avencall.com
XIVOCC_IP=192.168.32.15
XIVO_IP=192.168.32.10
MDS1_IP=192.168.32.12
MDS2_IP=192.168.32.13
DEBUG=false
DEFAULT_USER_PWD=secret
TARGET_VERSION=master
CLIENT_PROXY_PORT=48576
WEBPACK_PORT=8115
DEBUG_PORT=9222
LOADTEST_URL=localhost
PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
PUPPETEER_EXECUTABLE_PATH=/puppeteer-controller/node_modules/puppeteer/.local-chromium/chrome-linux/chrome
```

File: `multiple_calls.scenario`

```
STARTING_PHONE_NUMBER=8998  
WEBRTC_LOADTEST_ITERATIONS=5
CALLEE_COMMON_PEER_ID=12504
CALLEE_COMMON_PEER_USERNAME=webrtc_common_callee
CALLEE_COMMON_PEER_NUMBER=8998
CALLER_COMMON_PEER_ID=12505
CALLER_COMMON_PEER_USERNAME=webrtc_common_caller
CALLER_COMMON_PEER_NUMBER=8999
SCENARIO_CONDITION=caller.EventReleased
BROWSER_MODE=0
NUMBER_OF_USERS=2
AVERAGE_TIME_TO_CALL_MS=120000
DELAY=1000
CALL_LENGTH_MS=60000
DELAY_BETWEEN_CALLS=3000
```