#!/bin/bash

ENV_FILE_PATH="./.env"
DOCKER_COMPOSE_FILE="./docker-compose.yml"
CONFIG_PATH="./config"
TEMPLATES_PATH="./templates"
LOADTEST_CONFIG_PATH="$CONFIG_PATH/load_test_config"
COMPOSE_TEMPLATE="$TEMPLATES_PATH/compose_template"
SERVICE_TEMPLATE="$TEMPLATES_PATH/puppeteer_service_template_gpu"
IDENTATION="      - "
DOCKER_COMPOSE="docker compose"

function check_base_load_test_config() {
    if [ ! -f "$LOADTEST_CONFIG_PATH" ]; then
        echo "Missing base load test configuration file $LOADTEST_CONFIG_PATH!"
        exit 1
    else
        echo "Found base load test configuration file $LOADTEST_CONFIG_PATH."
    fi
}

function check_scenarios_config() {
    scenario_count=`ls $CONFIG_PATH | grep -E -i '^(.*)\.scenario$' | wc -l`
    if [ $scenario_count -eq 0 ]; then
        echo "No scenario configuration file found in $CONFIG_PATH!"
        echo "Note: make sure that configuration files for different scenarios have .scenario extension."
        exit 1
    else
        echo "Found $scenario_count scenario configuration files."
    fi
}

function recreate_compose_and_env_files() {
    echo "Recreating docker compose and env files."
    rm -rf $ENV_FILE_PATH $DOCKER_COMPOSE_FILE
    touch $ENV_FILE_PATH
    cp $COMPOSE_TEMPLATE $DOCKER_COMPOSE_FILE
}

function add_base_load_test_config_vars_to_env_file() {
    echo "Adding base load test configuration variables to env file."
    cat $LOADTEST_CONFIG_PATH >> $ENV_FILE_PATH
}

function add_scenario_list_env_var(){
    echo "  Adding scenario configuration var containing scenario list to env file."
    echo $'\n' >> $ENV_FILE_PATH
    echo $'SCENARIOS='$1'' >> $ENV_FILE_PATH
}

function add_service_for_scenario_in_compose_file() {
    echo "  Adding puppeteer service for scenario $scenario_name to env file."
    cat "$SERVICE_TEMPLATE" >> "$DOCKER_COMPOSE_FILE"
    sed -i -e "s/puppeteer:/puppeteer_$scenario_name:/" "$DOCKER_COMPOSE_FILE"
}

function add_base_load_test_config_vars_to_service() {
    for env_var in `cat "$LOADTEST_CONFIG_PATH"`
    do
        var_name=`echo "$env_var" | cut -d "=" -f 1`
        echo "  Adding base load test configurations var: $var_name to current puppeteer service"
        echo "$IDENTATION$var_name=\${$var_name}" >> $DOCKER_COMPOSE_FILE
    done
}

function add_scenario_config_vars_to_service() {
    for env_var in `cat "$1"`
    do
        var_name=`echo "$env_var" | cut -d "=" -f 1`
        echo "  Adding scenario configuration var: $var_name for scenario $2 to current puppeteer service"
        scenario_prefix=`echo $2 | tr a-z A-Z`
        echo "$IDENTATION$var_name=\${${scenario_prefix}_${var_name}}" >> $DOCKER_COMPOSE_FILE
    done
}

function add_scenario_vars_to_env() {
    echo "  Adding scenario configuration vars into env file..."
    echo "      Creating a temporary env file with scenario configuration vars."
    local temporary_env="$CONFIG_PATH/$1.temp"
    local scenario_prefix=`echo $2 | tr a-z A-Z`
    echo "      Adding scenario prefix to scenario configuration vars in temporary env file."
    sed -e "s/^/${scenario_prefix}_/" "$CONFIG_PATH/$1" > $temporary_env
    echo "      Adding scenario configuration vars from temporary env file into the main env file."
    echo $'\n' >> $ENV_FILE_PATH
    cat $temporary_env >> $ENV_FILE_PATH
    echo "      Removing the temporary env file with scenario configuration vars."
    rm -rf "$temporary_env"
}

function add_scenario_name_var_to_service() {
    echo "  Adding scenario name var: SCENARIO with value: $1 to current puppeteer service."
    echo "${IDENTATION}SCENARIO=$1" >> $DOCKER_COMPOSE_FILE
}

function add_vars_to_services_and_env_file() {
    local scenarios=($(ls $CONFIG_PATH | grep -E -i '^(.*)\.scenario$'))
    local list_of_scenarios=""
    for scenario in "${scenarios[@]}"
    do
        local scenario_name=`echo "$scenario" | cut -d "." -f 1`
        echo "Going to include scenario configuration vars for scenario $scenario_name in docker compose and env file."
        add_service_for_scenario_in_compose_file "$scenario_name"
        add_base_load_test_config_vars_to_service
        add_scenario_config_vars_to_service "$CONFIG_PATH/$scenario" "$scenario_name"
        add_scenario_name_var_to_service "$scenario_name"
        add_scenario_vars_to_env $scenario $scenario_name
        if [ -z $list_of_scenarios ]; then list_of_scenarios="$scenario_name"; continue; fi
        list_of_scenarios="$list_of_scenarios,$scenario_name"
    done
    add_scenario_list_env_var "$list_of_scenarios"
}

function source_the_env_file() {
    echo "Sourcing env file now."
    source .env
}

function init_docker_compose_files() {
    check_base_load_test_config
    check_scenarios_config
    recreate_compose_and_env_files
    add_base_load_test_config_vars_to_env_file
    add_vars_to_services_and_env_file
    source_the_env_file
}

function init_logs() {
    echo "Initializing loadtest.log file."
    [ -f loadtest.log ] && rm -rf loadtest.log
    touch loadtest.log
}

function stop_containers() {
    echo "Stopping and removing exisitng webrtc_load_test_ docker containers."
    local docker_containers=$(docker container ls --all --format {{.Names}} | grep "webrtc_load_tests_")
    if [[ $docker_containers ]]; then docker stop $docker_containers && docker rm $docker_containers; fi
}

function webrtc_dcomp() {
    puppeteer_service=$($DOCKER_COMPOSE ps -a --services | grep -i "puppeteer" | head -n 1)
    if [ $# -eq 0 ]
    then
        $DOCKER_COMPOSE run --rm $puppeteer_service
    else
        $DOCKER_COMPOSE "$@"
    fi
}

function clean_environment() {
    init_docker_compose_files
    local TARGET_VERSION=$(cat .env | grep -oP 'TARGET_VERSION=\K(.*)')
    rm -rf webrtc_load_tests_puppeteer && git clone -b ${TARGET_VERSION} https://gitlab.com/xivo.solutions/webrtc_load_tests_puppeteer.git
    rm -rf webrtc_load_tests_webpack && git clone -b ${TARGET_VERSION} https://gitlab.com/xivo.solutions/webrtc_load_tests_webpack.git
    rm -rf webrtc_load_tests_client_proxy && git clone -b ${TARGET_VERSION} https://gitlab.com/xivo.solutions/webrtc_load_tests_client_proxy.git
    stop_containers
    local docker_images=$(docker image ls --all --format {{.Repository}} | grep "webrtc_load_tests_")
    if [[ $docker_images ]]; then docker rmi $docker_containers ; fi 
    webrtc_dcomp build 
}

function build_environment() {
    stop_containers
    init_docker_compose_files
    webrtc_dcomp build 
}

function update_loadtest() {
    stop_containers
    init_docker_compose_files
}

function start_loadtest() {
    check_base_load_test_config
    check_scenarios_config
    echo "Starting webrtc load test puppeteer containers now."
    webrtc_dcomp
}

function display_help() {
    echo "usage:
    no parameter    - starts the containers.
    -u --update     - generates new configuration and stops all containers (production).
    -b --build      - updates containers with new configuration and code changes and stops all containers (development).
    -c --clean      - updates containers with new version (production).
    -h --help       - shows this message."
    exit 0
}

if [ -z "$1" ]; then 
    start_loadtest
elif [ "$1" == "-u" ] || [ "$1" == "--update" ]; then 
    update_loadtest
elif [ "$1" == "-b" ] || [ "$1" == "--build" ]; then
    build_environment
elif [ "$1" == "-c" ] || [ "$1" == "--clean" ]; then
    clean_environment
elif [ "$1" == '-h' ] || [ "$1" == '--help' ]; then 
    display_help
else
    echo "Unknown parameter."
    exit 1
fi
